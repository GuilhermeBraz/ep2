package snakegME;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;
import java.util.TimerTask;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.JPanel;

public class Gameplay extends JPanel implements KeyListener,ActionListener{

	private int[] snakexlength = new int[750];
	private int[] snakeylength = new int[750];

	private int snaketype;
	private int enemytype;
	protected String image = "images/enemy.png";
	
	private Snake snake;

	//private int[] snaketype = {0,1,2};
	//private int snaketype;
	private int[] enemytypeR = {0,1,2,3};

	private boolean left = false;
	private boolean right = false;
	private boolean up = false;
	private boolean down = false;

	private boolean running = true;
	private long tempo = 0;
	private long barreartempo = 0;

	private ImageIcon rightmouth;
	private ImageIcon upmouth;
	private ImageIcon downmouth;
	private ImageIcon leftmouth;

	private int lengthofsnake = 3;
	private int moves = 0;
	private int score = 0;

	private int[] enemyxpos={25,50,75,100,125,150,175,200,225,250,275,300,//12
			325,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,//17
			775,800,825,850};//4
	private int[] enemyypos= {75,100,125,150,175,200,225,250,275,300,
			325,375,400,425,450,475,500,525,550,575,600,625};

	private int[] barrearxpos={25,50,75,100,125,150,175,200,225,250,275,300,//12
			325,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,//17
			775,800,825,850};//4
	private int[] barrearypos= {75,100,125,150,175,200,225,250,275,300,
			325,375,400,425,450,475,500,525,550,575,600,625};
	
	private int[] barrear1xpos={25,50,75,100,125,150,175,200,225,250,275,300,//12
			325,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,//17
			775,800,825,850};//4
	private int[] barrear1ypos= {75,100,125,150,175,200,225,250,275,300,
			325,375,400,425,450,475,500,525,550,575,600,625};

	private int[] barrear2xpos={25,50,75,100,125,150,175,200,225,250,275,300,//12
		325,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,//17
		775,800,825,850};//4
	private int[] barrear2ypos= {75,100,125,150,175,200,225,250,275,300,
		325,375,400,425,450,475,500,525,550,575,600,625};
	
	private ImageIcon enemyimage;
	private String enemy;

	private Random random = new Random();

	private int xpos = random.nextInt(33);
	private int ypos = random.nextInt(22);

	private int bxpos = random.nextInt(33);
	private int bypos = random.nextInt(22);

	private int b1xpos = random.nextInt(33);
	private int b1ypos = random.nextInt(22);

	private int b2xpos = random.nextInt(33);
	private int b2ypos = random.nextInt(22);

	private Timer timer;
	private int delay = 100;
	private ImageIcon snakeimage;

	private ImageIcon titleImage;

	//construtor basico
	public Gameplay()
	{
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		timer = new Timer(delay, this);
		timer.start();
		snake = new Snake();
	}

	//construtor 
	public Gameplay(int snaketype)
	{
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		timer = new Timer(delay, this);
		timer.start();
		if(snaketype == 1)
		{
			snake = new Snake(true);
		}else if(snaketype == 2)
		{
			snake = new Snake(false,true);
		}


	}

	public void paint(Graphics g)
	{
		if(moves == 0)
		{
			snakexlength[2]= 50;
			snakexlength[1]=75;
			snakexlength[0]=100;

			snakeylength[2]= 100;
			snakeylength[1]=100;
			snakeylength[0]=100;
		}


		// draw title image boarder
		g.setColor(Color.white);
		g.drawRect(24,10,851,55);

		titleImage = new ImageIcon("images/snaketitle.jpg");
		titleImage.paintIcon(this, g, 25, 11);

		//draw boarder for gameplay 
		g.setColor(Color.white);
		g.drawRect(24, 74, 851, 577);

		//draw background for gameplay
		g.setColor(Color.black);
		g.fillRect(25, 75, 850, 575);


		//draw scores
		g.setColor(Color.white);
		g.setFont(new Font(	"arial",Font.PLAIN, 14));
		g.drawString("Scores: "+score, 780 , 30);

		//draw length of snake
		g.setColor(Color.white);
		g.setFont(new Font(	"arial",Font.PLAIN, 14));
		g.drawString("Length: "+lengthofsnake, 780 , 50);

		rightmouth = new ImageIcon(snake.getRightmouth());
		//outros tipos
		rightmouth.paintIcon(this, g, snakexlength[0],snakeylength[0] );

		for(int a= 0; a<lengthofsnake;a++)
		{
			if(a==0 && right)
			{	
				rightmouth = new ImageIcon(snake.getRightmouth());
				//outros tipos 
				rightmouth.paintIcon(this, g, snakexlength[a],snakeylength[a] );
			}

			if(a==0 && left)
			{
				leftmouth = new ImageIcon(snake.getLeftmouth());
				//outros tipos
				leftmouth.paintIcon(this, g, snakexlength[a],snakeylength[a] );
			}

			if(a==0 && down)
			{
				downmouth = new ImageIcon(snake.getDownmouth());
				//outros tipos
				downmouth.paintIcon(this, g, snakexlength[a],snakeylength[a] );
			}

			if(a==0 && up)
			{
				upmouth = new ImageIcon(snake.getUpmouth());
				//outros tipos
				upmouth.paintIcon(this, g, snakexlength[a],snakeylength[a] );
			}

			if(a!= 0)
			{
				snakeimage = new ImageIcon(snake.getSnakeimage());
				//outros tipos
				snakeimage.paintIcon(this, g, snakexlength[a],snakeylength[a] );
			}

		}

		//detect collison with apple






	
		
		Runnable r = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				//timer  

					if(tempo == 85)
					{
						tempo = 0;
					}
					if(barreartempo == 50)
					{	
						bxpos = random.nextInt(33);
						bypos = random.nextInt(22);
						
						b1xpos = random.nextInt(33);
						b1ypos = random.nextInt(22);
						
						b2xpos = random.nextInt(33);
						b2ypos = random.nextInt(22);
						barreartempo = 0;
					}	
						barreartempo++;
						tempo++;
						System.out.println(tempo);
						
			}
					
			
			
		};
		Thread counter = new Thread(r);
		counter.start();
		
	
		
		
		ImageIcon box = new ImageIcon("images/box.png");
		enemyimage = new ImageIcon(image);
		
		if(((enemyxpos[xpos] == snakexlength[0]) && enemyypos[ypos] == snakeylength[0]) || tempo == 85 )
		{	
			if(tempo == 85)
			{
				
			}
			else
			{	
				if(image == "images/enemy2.png")
				{
					score+=2;
				}
				if(image == "images/enemy3.png")
				{
					right = false;
					left = false;
					up = false;
					down = false;
					running = false;

					g.setColor(Color.white);
					g.setFont(new Font("arial",Font.BOLD, 50));
					g.drawString("Game Over",300, 300);

					g.setFont(new Font("arial", Font.BOLD, 20));
					g.drawString("Space to RESTART", 350,340);

				}
				if(image == "images/enemy.png")
				{
					score++;
				}
				if(snake.isDouble_points()) {
					if(image != "images/enemy4.png") {	
						score ++;
					}
					if(image == "images/enemy2.png")
					{
						score++;
					}
				}	
				lengthofsnake++;
				
				if(image == "images/enemy4.png")
				{	
					//moves = 0;
					lengthofsnake = 3;
				}
			}
			tempo = 0;
			xpos = random.nextInt(33);
			ypos = random.nextInt(22);
			enemytype = random.nextInt(4);	
			if(enemytype == 0)
			{
				image = "images/enemy.png";
			}
			else if(enemytype == 1)
			{
				image = "images/enemy2.png";
			}
			else if(enemytype == 2)
			{
				image = "images/enemy3.png";
			}
			else if(enemytype == 3)
			{
				image = "images/enemy4.png";
			}
		}

		if((barrearxpos[bxpos] == snakexlength[0]) && barrearypos[bypos] == snakeylength[0])
		{	
			if(snake.isBarrear_break())
			{

			}
			else
			{
				right = false;
				left = false;
				up = false;
				down = false;
				running = false;

				g.setColor(Color.white);
				g.setFont(new Font("arial",Font.BOLD, 50));
				g.drawString("Game Over",300, 300);

				g.setFont(new Font("arial", Font.BOLD, 20));
				g.drawString("Space to RESTART", 350,340);
			}
			bxpos = random.nextInt(33);
			bypos = random.nextInt(22);
		}

		if((barrear1xpos[b1xpos] == snakexlength[0]) && barrear1ypos[b1ypos] == snakeylength[0])
		{	
			if(snake.isBarrear_break())
			{

			}
			else
			{
				right = false;
				left = false;
				up = false;
				down = false;
				running = false;

				g.setColor(Color.white);
				g.setFont(new Font("arial",Font.BOLD, 50));
				g.drawString("Game Over",300, 300);

				g.setFont(new Font("arial", Font.BOLD, 20));
				g.drawString("Space to RESTART", 350,340);
			}
			b1xpos = random.nextInt(33);
			b1ypos = random.nextInt(22);
		}

		if((barrear2xpos[b2xpos] == snakexlength[0]) && barrear2ypos[b2ypos] == snakeylength[0])
		{	
			if(snake.isBarrear_break())
			{

			}
			else
			{
				right = false;
				left = false;
				up = false;
				down = false;
				running = false;

				g.setColor(Color.white);
				g.setFont(new Font("arial",Font.BOLD, 50));
				g.drawString("Game Over",300, 300);

				g.setFont(new Font("arial", Font.BOLD, 20));
				g.drawString("Space to RESTART", 350,340);
			}
			b2xpos = random.nextInt(33);
			b2ypos = random.nextInt(22);
		}
		box.paintIcon(this,g,barrearxpos[bxpos],barrearypos[bypos]);
		box.paintIcon(this,g,barrear1xpos[b1xpos],barrear1ypos[b1ypos]);
		box.paintIcon(this,g,barrear2xpos[b2xpos],barrear2ypos[b2ypos]);
		enemyimage.paintIcon(this,g,enemyxpos[xpos],enemyypos[ypos]);

		for (int b = 1; b<lengthofsnake; b++)
		{
			if(snakexlength[b] == snakexlength[0] && snakeylength[b] == snakeylength[0])
			{
				right = false;
				left = false;
				up = false;
				down = false;
				running = false;

				g.setColor(Color.white);
				g.setFont(new Font("arial",Font.BOLD, 50));
				g.drawString("Game Over",300, 300);

				g.setFont(new Font("arial", Font.BOLD, 20));
				g.drawString("Space to RESTART", 350,340);

			}	
		}


		g.dispose();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		timer.start();
		if(running)
		{
			//right
			if(right)
			{
				for (int r = lengthofsnake-1 ; r>=0 ; r--)
				{
					snakeylength[r+1] = snakeylength[r];
				}

				for (int r = lengthofsnake ; r>=0 ; r--)
				{
					if(r==0)
					{
						snakexlength[r]=snakexlength[r]+25;
					}	
					else
					{
						snakexlength[r]=snakexlength[r-1];
					}
					if (snakexlength[r]>850) 
					{
						snakexlength[r]=25;
					}
				}

				//paint
				repaint();
			}


			//left
			if(left)
			{
				for (int r = lengthofsnake-1 ; r>=0 ; r--)
				{
					snakeylength[r+1] = snakeylength[r];
				}

				for (int r = lengthofsnake ; r>=0 ; r--)
				{
					if(r==0)
					{
						snakexlength[r]=snakexlength[r]-25;
					}	
					else
					{
						snakexlength[r]=snakexlength[r-1];
					}
					if (snakexlength[r]<25) 
					{
						snakexlength[r]=850;
					}
				}
				//paint
				repaint();
			}
			//up
			if(up)
			{
				for (int r = lengthofsnake-1 ; r>=0 ; r--)
				{
					snakexlength[r+1] = snakexlength[r];
				}

				for (int r = lengthofsnake ; r>=0 ; r--)
				{
					if(r==0)
					{
						snakeylength[r]=snakeylength[r]-25;
					}	
					else
					{
						snakeylength[r]=snakeylength[r-1];
					}
					if (snakeylength[r]<75) 
					{
						snakeylength[r]=625;
					}
				}
				//paint
				repaint();
			}
			//down
			if(down)
			{
				for (int r = lengthofsnake-1 ; r>=0 ; r--)
				{
					snakexlength[r+1] = snakexlength[r];
				}

				for (int r = lengthofsnake ; r>=0 ; r--)
				{
					if(r==0)
					{
						snakeylength[r]=snakeylength[r]+25;
					}	
					else
					{
						snakeylength[r]=snakeylength[r-1];
					}
					if (snakeylength[r]>625) 
					{
						snakeylength[r]=75;
					}
				}
				//paint
				repaint();
			}

		}
		else
		{

		}
	}

	@Override
	public void keyPressed(KeyEvent e) {

		if(running)
		{
			//right
			if(e.getKeyCode() == KeyEvent.VK_RIGHT && (!left))
			{
				moves++;
				right = true;
				left = false;
				up = false;
				down = false;
			}
			//left
			if(e.getKeyCode() == KeyEvent.VK_LEFT)
			{
				moves++;
				left = true;
				if(!right)
				{
					left = true; 
				}
				else
				{
					left = false;
					right = true;
				}

				up = false;
				down = false;
			}
			//up
			if(e.getKeyCode() == KeyEvent.VK_UP)
			{
				moves++;
				up = true;
				if(!down)
				{
					up = true; 
				}
				else
				{
					up = false;
					down = true;
				}

				left = false;
				right = false;
			}
			//down
			if(e.getKeyCode() == KeyEvent.VK_DOWN)
			{
				moves++;
				down = true;
				if(!up)
				{
					down = true; 
				}
				else
				{
					down = false;
					up = true;
				}

				left = false;
				right = false;
			}
		}
		else
		{
			//space
			if(e.getKeyCode() == KeyEvent.VK_SPACE)
			{
				moves = 0;
				score = 0;
				lengthofsnake = 3;
				running = true;
				image = "images/enemy.png";
				repaint();
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	public String getEnemy() {
		return enemy;
	}

	public void setEnemy(String enemy) {
		this.enemy = enemy;
	}



}
