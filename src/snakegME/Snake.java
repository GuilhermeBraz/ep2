package snakegME;

import javax.swing.ImageIcon;

public class Snake {

	private int snaketype;
	private String snakeimage;
	private String rightmouth;
	private String leftmouth;
	private String upmouth;
	private String downmouth;
	private boolean double_points = false;
	private boolean barrear_break = false;

	public Snake()//without paramaters tradicional snake
	{
		this.snakeimage = "images/snakeimage.png";
		this.rightmouth = "images/rightmouth.png";
		this.leftmouth = "images/leftmouth.png";
		this.upmouth = "images/upmouth.png";
		this.downmouth ="images/downmouth.png";

	}

	public Snake(boolean barrear_break)//with 1 paramater kitty snake(white) - breaks barrear
	{
		this.snakeimage = "images/snakeimage2.png";
		this.rightmouth = "images/rightmouth2.png";
		this.leftmouth = "images/leftmouth2.png";
		this.upmouth = "images/upmouth2.png";
		this.downmouth ="images/downmouth2.png";
		this.barrear_break = barrear_break;

	}

	public Snake(boolean barrear_break,boolean double_points)//with 1 paramater kitty snake(white) - breaks barrear
	{
		this.snakeimage = "images/snakeimage3.png";
		this.rightmouth = "images/rightmouth3.png";
		this.leftmouth = "images/leftmouth3.png";
		this.upmouth = "images/upmouth3.png";
		this.downmouth ="images/downmouth3.png";
		this.barrear_break = false;
		this.double_points = double_points;


	}
	public int getSnaketype() {
		return snaketype;
	}



	public String getSnakeimage() {
		return snakeimage;
	}


	public String getRightmouth() {
		return rightmouth;
	}



	public String getLeftmouth() {
		return leftmouth;
	}


	public String getUpmouth() {
		return upmouth;
	}

	public String getDownmouth() {
		return downmouth;
	}


	public boolean isDouble_points() {
		return double_points;
	}


	public boolean isBarrear_break() {
		return barrear_break;
	}





}


