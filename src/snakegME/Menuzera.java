package snakegME;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.Random;

import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.ImageIcon;

import java.awt.event.ActionListener;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JList;

public class Menuzera {

	private JFrame frame;
	private int snaketype;
	private int[] type = {0,1,2};
	private Random random = new Random();
	//private ImageIcon snakebackgorund = new ImageIcon("images/snakebackground.jpg");
	private final Action action = new SwingAction();
	private final Action action_1 = new SwingAction_1();
	private final Action action_2 = new SwingAction_2();
	private final Action action_3 = new SwingAction_3();
	//public Menu menu = new Menu();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menuzera window = new Menuzera();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Menuzera() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		

		JButton btnStart = new JButton("START");
		btnStart.setAction(action);
		btnStart.setBounds(160, 80, 114, 25);
		frame.getContentPane().add(btnStart);

		JLabel lblMenu = new JLabel("MENU");
		lblMenu.setBounds(169, 12, 105, 40);
		lblMenu.setFont(new Font("arial",Font.PLAIN, 34));
		frame.getContentPane().add(lblMenu);

		JButton btnHelp = new JButton("normal");
		btnHelp.setAction(action_1);
		btnHelp.setBounds(39, 136, 114, 25);
		frame.getContentPane().add(btnHelp);

		JButton btnKitty = new JButton("kitty");
		btnKitty.setAction(action_2);
		btnKitty.setBounds(165, 136, 114, 25);
		frame.getContentPane().add(btnKitty);

		JButton btnStar = new JButton("star");
		btnStar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnStar.setAction(action_3);
		btnStar.setBounds(291, 136, 114, 25);
		frame.getContentPane().add(btnStar);


		//ImageIcon snakebackground = new ImageIcon("images/snakebackground.jpg");

		snaketype = random.nextInt(3);
	}
	public int getSnaketype() {
		return snaketype;
	}

	public void setSnaketype(int snaketype) {
		this.snaketype = snaketype;
	}
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "START");
			putValue(SHORT_DESCRIPTION, "Starts game with the brevious choosen snake (if nothing choosen-> random snake");
		}
		public void actionPerformed(ActionEvent e) {
			frame.setVisible(false);
			
			JFrame game = new JFrame();
			game.setBounds(500,100,905,700);
			game.setBackground(Color.DARK_GRAY);
			game.setResizable(false);
			game.setVisible(true);
			game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


			if(getSnaketype()== 0)
			{
				Gameplay gameplay = new Gameplay();
				game.getContentPane().add(gameplay);
			}
			else if(getSnaketype() == 1)
			{
				Gameplay gameplay = new Gameplay(1);
				game.getContentPane().add(gameplay);
			}
			else if(getSnaketype() == 2)
			{
				Gameplay gameplay = new Gameplay(2);
				game.getContentPane().add(gameplay);
			}


		}
	}
	private class SwingAction_1 extends AbstractAction {
		public SwingAction_1() {
			putValue(NAME, "normal");
			putValue(SHORT_DESCRIPTION, "normal snake");
		}
		public void actionPerformed(ActionEvent e) {
			setSnaketype(0);
		}
	}
	private class SwingAction_2 extends AbstractAction {
		public SwingAction_2() {
			putValue(NAME, "kitty");
			putValue(SHORT_DESCRIPTION, "break barrear");
		}
		public void actionPerformed(ActionEvent e) {
			setSnaketype(1);
		}
	}
	private class SwingAction_3 extends AbstractAction {
		public SwingAction_3() {
			putValue(NAME, "star");
			putValue(SHORT_DESCRIPTION, "double points");
		}
		public void actionPerformed(ActionEvent e) {
			setSnaketype(2);
		}
	}
}
